package br.com.itau.investimento.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.investimento.models.Simulacao;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer>{

}
