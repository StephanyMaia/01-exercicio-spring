package br.com.itau.investimento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.services.AplicacaoService;

@RestController
@RequestMapping("/aplicacao")
public class AplicacaoController {

	@Autowired
	AplicacaoService aplicacaoService;
	
	@GetMapping
	public Iterable<Aplicacao> listarAplicacao() {
		return aplicacaoService.buscarAplicacao();
	}
	
	@PostMapping
	public void inserirAplicacao(@RequestBody Aplicacao aplicacao) {
		aplicacaoService.inserirAplicacao(aplicacao);
	}
}
