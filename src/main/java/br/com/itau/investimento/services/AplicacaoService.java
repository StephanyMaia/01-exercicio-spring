	package br.com.itau.investimento.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.repositories.AplicacaoRepository;

@Service
public class AplicacaoService {
	@Autowired
	AplicacaoRepository aplicacaoRepository;
	
	public Iterable<Aplicacao> buscarAplicacao(){
		return aplicacaoRepository.findAll();
	}
	
	public void inserirAplicacao(@RequestBody Aplicacao aplicacao) {
		aplicacaoRepository.save(aplicacao);
	}

}
