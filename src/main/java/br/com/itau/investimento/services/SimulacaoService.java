package br.com.itau.investimento.services;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.repositories.ProdutoRepository;
import br.com.itau.investimento.repositories.SimulacaoRepository;

@Service
public class SimulacaoService {
	
	@Autowired
	SimulacaoRepository simulacaoRepository;

	@Autowired
	ProdutoRepository produtoRepository;

	public List<Simulacao> calcular(Produto produto, double valor, int quantidadeMeses){
		List<Simulacao> simulacoes = new ArrayList<Simulacao>();
		Locale locale = new Locale("pt", "BR");

		for(int i = 0; i <= quantidadeMeses; i++) {
			Simulacao simulacao = new Simulacao();
			
			Month mes = Month.JANUARY;
			simulacao.setMes(mes.getDisplayName(TextStyle.FULL, locale));
			simulacao.setValor(valor);
			
			valor += valor * produto.getRendimento();
			
			simulacoes.add(simulacao);

		}
		return simulacoes;
	}
}
