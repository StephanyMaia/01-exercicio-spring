package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.repositories.ProdutoRepository;

@Service
public class CatalogoService {

	@Autowired
	ProdutoRepository produtoRepository;
	
	public Iterable<Produto> obterProdutos(){
		return produtoRepository.findAll();
	}
	
	public Optional<Produto> obterProdutoPorId(@PathVariable int id) {
		return produtoRepository.findById(id);
	}
}
