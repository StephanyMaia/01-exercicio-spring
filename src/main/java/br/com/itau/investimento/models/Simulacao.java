package br.com.itau.investimento.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Simulacao {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int idSimulacao;
	public String mes;
	public double valor;
	
	public int getIdSimulacao() {
		return idSimulacao;
	}
	public void setIdSimulacao(int idSimulacao) {
		this.idSimulacao = idSimulacao;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
}
